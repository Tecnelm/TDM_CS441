package fr.esisar.calculatrice;

/**
 * Class test of Calculatrice
 * 
 * @author clement
 *
 */
public class Calculateur {

	public static void main(String[] args) throws OperationInvalide {
		
		Calculatrice calc = new Calculatrice();

		Double op1 = 10.;
		Double op2 = 5.;

		Double addResult = calc.calculer(op1, op2,(a,b) -> a+b);
		Double subResult = calc.calculer(op1, op2 ,(a,b) -> a-b);
		Double multResult = calc.calculer( op1, op2,(a,b) -> a*b);
		Double divideResult = calc.calculer(op1, op2,(a,b) -> {
			if (b ==0)
				throw new OperationInvalide("Division par 0");
			return a/b;
		});
		
		System.err.println("Réalisation opération ajouter résultat: " + (addResult == 15));
		System.err.println("Réalisation opération soustraction résultat: " + (subResult == 5));
		System.err.println("Réalisation opération multiplication résultat: " + (multResult == 50));
		System.err.println("Réalisation opération division résultat: " + (divideResult == 2));
		
		

	}

}
