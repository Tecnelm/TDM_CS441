package fr.esisar.calculatrice;

/**
 * 
 * @author clement
 *
 */
public class Calculatrice {
	/**
	 * 
	 */
	
	@FunctionalInterface
	interface OperationDouble
	{
		Double doCalculer(Double op1, Double op2) throws OperationInvalide;
	}
	public Calculatrice() {
	}
	
	public Double calculer(Double op1 , Double op2, OperationDouble operation) throws OperationInvalide {
		return operation.doCalculer(op1, op2);
	}

}
