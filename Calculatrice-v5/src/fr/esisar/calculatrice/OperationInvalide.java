package fr.esisar.calculatrice;

/**
 * 
 * @author clement
 *
 */
public class OperationInvalide extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	/**
	 * 
	 * @param message
	 */
	public OperationInvalide(String message)
	{
		this.message = message;
	}
	
	@Override
	public String toString()
	{
		return this.message;
	}

}
