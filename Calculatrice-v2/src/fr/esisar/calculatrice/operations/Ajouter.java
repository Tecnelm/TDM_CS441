package fr.esisar.calculatrice.operations;

import fr.esisar.calculatrice.CalculatriceException;

/**
 * 
 * @author clement
 *
 */
public class Ajouter implements Operation {

	@Override
	public String getNom() {
		return "+";
	}

	@Override
	public Double calculer(Double operande1, Double operande2) throws CalculatriceException {
		return operande1 + operande2;
	}

}
