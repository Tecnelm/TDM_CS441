package fr.esisar.calculatrice.operations;

import fr.esisar.calculatrice.CalculatriceException;

/**
 * 
 * @author clement
 *
 */
public interface Operation {
	/**
	 * 
	 * @return String that correspond to the char of the operation
	 */
	public String getNom();

	/**
	 * Calculate result of the operation that methode
	 * 
	 * @param operande1
	 * @param operande2
	 * @return result of operation
	 */
	public Double calculer(Double operande1, Double operande2) throws CalculatriceException;

}
