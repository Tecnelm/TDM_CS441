package fr.esisar.calculatrice;

import java.util.Set;

import fr.esisar.calculatrice.operations.Operation;

/**
 * 
 * @author clement
 *
 */
public class Calculatrice {

	private Set<Operation> opL;

	public Calculatrice() {
	}
	public Calculatrice(Set<Operation> operations)
	{
		this.opL = operations;
	}

	/**
	 * Cherche l'opération correspondant au nom et la retourne
	 * 
	 * @param nom str that correspond to the operation
	 * @return Operation correspondant au nom
	 * @throws CalculatriceException en cas d'operation inexistante
	 */
	public Operation chercherOperation(String nom) throws CalculatriceException {
		for (Operation o : opL) {
			if (o.getNom().equals(nom)) {
				return o;
			}
		}
		throw new CalculatriceException("Operation non existante");
	}

	/**
	 * Calcule une expression avec le nom de l'opération et les deux opérande
	 * 
	 * @param nom
	 * @param operande1
	 * @param operande2
	 * @return résultat de l'opération
	 * @throws CalculatriceException
	 */
	public Double calculer(String nom, Double operande1, Double operande2) throws CalculatriceException {

		return this.chercherOperation(nom).calculer(operande1, operande2);
	}

}

/**
 *  Ordre -> package -> Interface -> abstract -> class
 *  
 *  L'interface operation sert à avoir la présence de methode  sans pour autant 
 *  avoir a étendre une autre classe
 *  
 *  ReadOnly signifie que la variable ne peut que être lu et pas modifier, ceci se représente avec uniquement un getter 
 *  
 *  La calculatrice à toutes les operations de sauvegardé avec un Set(liste d'élement unique) 
 *  Ses attribus sont un Set d'opération 
 *  
 *  deux méthodes :
 *  -Constructeur statique -> crée le set et met des operations de base  -> methode  permettant l'ajout d'opération
 *  -Dynamique -> prend en argument un set d'opération qu'il utilisera -> methode  permettant l'ajout d'opération 
 */