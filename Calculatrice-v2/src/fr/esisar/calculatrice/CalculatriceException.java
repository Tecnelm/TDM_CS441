package fr.esisar.calculatrice;

public class CalculatriceException extends Exception {


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5991714585696464099L;
	private final String message;

	public CalculatriceException(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return this.message;
	}

}
