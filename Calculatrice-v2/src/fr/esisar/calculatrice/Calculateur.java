package fr.esisar.calculatrice;

import java.util.HashSet;
import java.util.Set;

import fr.esisar.calculatrice.operations.Ajouter;
import fr.esisar.calculatrice.operations.Diviser;
import fr.esisar.calculatrice.operations.Multiplier;
import fr.esisar.calculatrice.operations.Operation;
import fr.esisar.calculatrice.operations.Soustraire;

/**
 * Class test of Calculatrice
 * 
 * @author clement
 *
 */
public class Calculateur {

	public static void main(String[] args) throws CalculatriceException {
		
		Set<Operation> opl = new HashSet<Operation>();
		opl.add(new Ajouter());
		opl.add(new Diviser());		
		opl.add(new Soustraire());
		opl.add(new Multiplier());

		Calculatrice calc = new Calculatrice(opl);

		Double op1 = 10.;
		Double op2 = 5.;

		Double addResult = calc.calculer("+", op1, op2);
		Double subResult = calc.calculer("-", op1, op2);
		Double multResult = calc.calculer("*", op1, op2);
		Double divideResult = calc.calculer("/", op1, op2);
		
		System.err.println("Réalisation opération ajouter résultat: " + (addResult == 15));
		System.err.println("Réalisation opération soustraction résultat: " + (subResult == 5));
		System.err.println("Réalisation opération multiplication résultat: " + (multResult == 50));
		System.err.println("Réalisation opération division résultat: " + (divideResult == 2));

	}

}
