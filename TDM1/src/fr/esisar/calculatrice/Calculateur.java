package fr.esisar.calculatrice;

/**
 * Test calculatrice
 * @author Garrigues Clément
 * @version 1.0
 */
public class Calculateur {

	public static void main(String[] args) {
			
		Integer op1 = 10;
		Integer op2 = 5;
		Calculatrice cal = new Calculatrice();
		
		Integer addResult = cal.addition(op1, op2);
		Integer subResult = cal.soustraction(op1, op2);
		Integer multResult = cal.multiplication(op1, op2);
		Float divideResult = cal.division(op1, op2);
		
		
		System.err.println("Réalisation opération ajouter résultat: "+ (addResult == 15));
		System.err.println("Réalisation opération soustraction résultat: "+ (subResult == 5));
		System.err.println("Réalisation opération multiplication résultat: "+ (multResult == 50));
		System.err.println("Réalisation opération division résultat: "+ (divideResult == 2));
		
	}

}
