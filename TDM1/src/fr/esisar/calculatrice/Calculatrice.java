package fr.esisar.calculatrice;

public class Calculatrice {

	/**
	 * realise addition between two operande
	 * 
	 * @param operande1 Integer premier operande
	 * @param operande2 Integer second operande
	 * @return Integer result of add operation
	 */
	public Integer addition(Integer operande1, Integer operande2) {
		return operande1 + operande2;
	}

	/**
	 * realise subtitude between two operande
	 * 
	 * @param operande1 Integer premier operande
	 * @param operande2 Integer second operande
	 * @return Integer result of substrate operation
	 */
	public Integer soustraction(Integer operande1, Integer operande2) {
		return operande1 - operande2;
	}

	/**
	 * realise multiplication between two operande
	 * 
	 * @param operande1 Integer premier operande
	 * @param operande2 Integer second operande
	 * @return Integer result of multiplication operation
	 */
	public Integer multiplication(Integer operande1, Integer operande2) {
		return operande1 * operande2;
	}

	/**
	 * realise division between two operande
	 * 
	 * @param operande1 Integer premier operande
	 * @param operande2 Integer second operande
	 * @return Float result of divide operation
	 */
	public Float division(Integer operande1, Integer operande2) {
		return operande1.floatValue() / operande2.floatValue();
	}

}
