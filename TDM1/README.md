
# TDM1 CS441
   L'objectif de ce TDM est de configurer L'ide eclipse afin d'utiliser le bon interpréteur lors de la réalisation d'un projet. 
   Les outils de base de compilation à la ligne de commande, javaDOC, configuration jmv eclipse seront abordé
## Configuration Eclipse
	
* chemin basic vers la jvm:
	`/usr/lib/jvm/java-11-openjdk-amd64`
 * Attention il faut correctement setup comment eclipse doit traiter le code (version de compilation java)
 * Nommage des package:
	 * entièrement en minuscule 
	 * représente l'arboressance des fichiers crée
	 * `fr.esisar.calculatrice`

## Réalisation de la calculatrice
* Faire attention a utiliser les Integer (type primitif de java) ils permettent de rajouter des fonctions de conversion et rester en accord avec le langage objet qu'est le java 
* Pour la division renvoyer un Float, les conversion d'integer utiliser la méthode `.floatValue()`

## Compilation à la ligne de commande 

* Pour compiler les sources:
	* Crée un répertoire classes 
	* se placer dans le répertoire parent et éxécuter la commande suivante:
		* ```$ javac -d ./classes ./fr/esisar/calculatrice/*.java```
		* cette commande permet la compilation des .java en .class
	* Crée un fichier `Manifest.mf` et rajouter la ligne `Main-Class: fr.esisar.calculatrice.Calculateur`
	* se placer dans le répertoire classes et taper la commande suivante afin de réaliser un .jar
		* `jar cfm ../calculatrice-v01.jar ../Manifest.mf  fr/esisar/calculatrice/Calculat*.class`
		* Il est possible mais non necessaire de mettre les .java dans le .jar. 
## JavaDoc 

* Exemple de javadoc pour une méthode :
>     /**
>     	Valide un mouvement de jeu d'Echecs.
>       @param leDepuisFile   File de la pièce à déplacer
>       @param leDepuisRangée Rangée de la pièce à déplacer
>       @param leVersFile     File de la case de destination 
>       @param leVersRangée   Rangée de la case de destination 
>       @return vrai (true) si le mouvement d'échec est valide ou faux (false) sinon
>      */
>      boolean estUnDeplacementValide(int leDepuisFile, int leDepuisRangée, int leVersFile, int leVersRangée)
>      {
>          ...
>      }

* Exemple de javaDoc pour une class:

>     /**
>      * Classe de gestion d'étudiants
>      * @author John Doe
>      * @version 2.6
>      */
>      public class Etudiant
>      {
>        ...
>      }
## Les taches ANT
### Prérequis 
* il est nécessaire d'avoir s'intaller ant sur sa machine 
```bash 
sudo apt install ant 
 ``` 
 * Il peut être interessant de regarder la [documentation](https://www.jmdoudoux.fr/java/dej/chap-ant.htm) de ant.
	 * Permet de réaliser des routines (un peu comme un makefile) afin de compiler des programmes java, lancer des test, générer la documentation.
	 * Il est crossPlatform
	 * Il est au format xml
### Fonctionnement 
* On déclare le projet.
	* Les constantes
	* Les routines
		* les taches a réaliser dans les routines
* Exemple dans ce projet: 
```xml
<project name="Calculatrice" basedir="." default="run">

<property name="srcdir" value="src"/>
<property name="builddir" value="build"/>
<property name="classdir" value="${builddir}/classes"/>
<property name="jardir" value="${builddir}/jar"/>
<property name="docdir" value="./doc"/>

<property name="version" value="1.0"/>
<property name="mainClass" value="fr.esisar.calculatrice.Calculateur"/>


<target name="clean">
    <delete dir="${builddir}"/>
    <delete dir="${docdir}"/>

</target>

<target name="build" depends="clean">
    <mkdir dir="${classdir}" />
    <javac srcdir="${srcdir}" destdir="${classdir}" includeantruntime="false"/>
</target>

<target name="run" depends="package">
    <java jar="${jardir}/${ant.project.name}-v${version}.jar" fork="true"/>
</target>

<target name="package" depends="build">
    <mkdir dir="${jardir}"/>
    <jar destfile="${jardir}/${ant.project.name}-v${version}.jar" basedir="${classdir}">
        <manifest>
            <attribute name="Main-Class" value="${mainClass}"/>
        </manifest>
    </jar>
</target>

<target name="makeDoc" depends="build">
    <javadoc sourcepath="${srcdir}" destdir="${docdir}">
        <fileset dir="${srcdir}" defaultexludes="yes">
            <include name="**"/>
        </fileset>
    </javadoc>
    
</target>

</project>
```
