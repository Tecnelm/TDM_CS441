package fr.esisar.calculatrice;

/**
 * 
 * @author clement
 *
 */
public class CalculatriceException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5991714585696464099L;
	/**
	 * 
	 */
	
	private String message;
	
	/**
	 * 
	 * @param message
	 */
	public CalculatriceException(String message)
	{
		this.message = message;
	}
	
	@Override
	public String toString()
	{
		return this.message;
	}

}
