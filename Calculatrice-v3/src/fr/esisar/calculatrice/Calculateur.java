package fr.esisar.calculatrice;

import java.util.HashSet;
import java.util.Set;

import fr.esisar.calculatrice.operations.Operation;
import fr.esisar.calculatrice.operations.binaires.Ajouter;
import fr.esisar.calculatrice.operations.binaires.Diviser;
import fr.esisar.calculatrice.operations.binaires.Multiplier;
import fr.esisar.calculatrice.operations.binaires.Soustraire;
import fr.esisar.calculatrice.operations.ensemblistes.Maximum;
import fr.esisar.calculatrice.operations.ensemblistes.Minimum;
import fr.esisar.calculatrice.operations.unaires.Cosinus;
import fr.esisar.calculatrice.operations.unaires.Sinus;
import fr.esisar.calculatrice.operations.unaires.Tangente;
import fr.esisar.calculatrice.operations.unaires.ValeurAbsolue;

/**
 * Class test of Calculatrice
 * 
 * @author clement
 *
 */
public class Calculateur {

	public static void main(String[] args) throws CalculatriceException {
		
		Set<Operation> opl = new HashSet<Operation>();
		opl.add(new Ajouter());
		opl.add(new Diviser());		
		opl.add(new Soustraire());
		opl.add(new Multiplier());
		
		opl.add(new Tangente());
		opl.add(new Sinus());		
		opl.add(new Cosinus());
		opl.add(new ValeurAbsolue());
		
		opl.add(new Maximum());
		opl.add(new Minimum());		

		Calculatrice calc = new Calculatrice(opl);

		Double op1 = 10.;
		Double op2 = 5.;
		Double op3 = -3.;
		Double op4 = Math.PI;
		

		Double addResult = calc.calculer("+", op1, op2);
		Double subResult = calc.calculer("-", op1, op2);
		Double multResult = calc.calculer("*", op1, op2);
		Double divideResult = calc.calculer("/", op1, op2);
		
		Double absResult = calc.calculer("abs", op3);
		Double cosResult = calc.calculer("cos", op4);
		Double sinResult = calc.calculer("sin", op4);
		Double tanResult = calc.calculer("tan", op4);
		
		Double minResult = calc.calculer("min", op1,op2,op3,op4);
		Double maxResult = calc.calculer("max", op1,op2,op3,op4);
		
		
		System.err.println("Réalisation opération ajouter résultat: " + (addResult == 15));
		System.err.println("Réalisation opération soustraction résultat: " + (subResult == 5));
		System.err.println("Réalisation opération multiplication résultat: " + (multResult == 50));
		System.err.println("Réalisation opération division résultat: " + (divideResult == 2));
		
		System.err.println("Réalisation opération abs résultat: " + (absResult >=0));
		System.err.println("Réalisation opération cos résultat: " + (cosResult == Math.cos(op4)));
		System.err.println("Réalisation opération sin résultat: " + (sinResult == Math.sin(op4)));
		System.err.println("Réalisation opération tan résultat: " + (tanResult == Math.tan(op4)));
		
		System.err.println("Réalisation opération min résultat: " + (minResult == -3 ));
		System.out.println(minResult);
		System.err.println("Réalisation opération max résultat: " + (maxResult == 10));
		
	}

}
/**
 * Idem TP 2 
 * Les classe abstraite serve définir le comportement de la méthode calculer afin de vérifier si l'opération correspond bien dans le type 
 * (opération unaire,...) 
 * 
 * sans pour autant définir le comportement de l'opération a réaliser 
 * 
 * 
 * protected sers a rendre la méthode utilisable uniquement dans le package ou les fils, sans rendre cette méthode directement 
 * utilisable par la classe calculatrice et empêcher les potentiels erreurs 
 * 
 * Sert a garantir que l'objet est séralisable et lors d'une désérialisation celui ci est à la bonne version du code 
 * afin d'éviter de désérialiser des objets depreciated
 * 
 * 
 * */
