package fr.esisar.calculatrice.operations.unaires;

import fr.esisar.calculatrice.operations.OperationUnaire;

/**
 * Réalise l'opération valeur absolue 
 * @author clement
 *
 */
public class ValeurAbsolue extends OperationUnaire{

	@Override
	public String getNom()
	{
		return "abs";
	}
	
	@Override
	protected Double doCalculer(Double operande1) {
		
		return Math.abs(operande1);
	}
}
