package fr.esisar.calculatrice.operations;

import fr.esisar.calculatrice.CalculatriceException;

/**
 * 
 * @author clement
 *
 */
public abstract class OperationBinaire implements Operation {
	
	
	@Override
	public Double calculer(Double... operandes) throws CalculatriceException {
		
		if (operandes.length != 2)
		{
			throw new CalculatriceException("Nombre D'opérande != 2 pour une opération binaire");
		}
		return this.doCalculer(operandes[0], operandes[1]);
	}
	/**
	 * Réalise l'opération binaire
	 * 
	 * @param operande1
	 * @param operande2
	 * @return
	 * @throws CalculatriceException
	 */
	protected abstract Double doCalculer(Double operande1,Double operande2) throws CalculatriceException;
}
