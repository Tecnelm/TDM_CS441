package fr.esisar.calculatrice.operations;

import fr.esisar.calculatrice.CalculatriceException;

/**
 * 
 * @author clement
 *
 */
public abstract class OperationUnaire implements Operation {
	
	@Override
	public Double calculer(Double... operandes) throws CalculatriceException {
		
		if (operandes.length != 1)
		{
			throw new CalculatriceException("Nombre D'opérande != 1 pour une opération binaire");
		}
		return this.doCalculer(operandes[0]);
	}
	
	/**
	 * Réalise l'opération Unaire
	 * @param operande1
	 * @return
	 */
	protected abstract Double doCalculer(Double operande1);

}
