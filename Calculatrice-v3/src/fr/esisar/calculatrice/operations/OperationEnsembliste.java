package fr.esisar.calculatrice.operations;

import fr.esisar.calculatrice.CalculatriceException;

/**
 * 
 * @author clement
 *
 */
public abstract class OperationEnsembliste implements Operation{
	
	@Override
	public Double calculer(Double... operandes) throws CalculatriceException {
		
		if (operandes.length == 0)
		{
			throw new CalculatriceException("Ensemble vide pour une opération ensembliste");
		}
		return this.doCalculer(operandes[0], operandes[2]);
	}
	
	/**
	 * Réalise l'opération Ensembliste
	 * @param operande1
	 * @return
	 */
	protected abstract Double doCalculer(Double... operande1);

}
