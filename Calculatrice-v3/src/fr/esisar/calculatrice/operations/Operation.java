package fr.esisar.calculatrice.operations;

import fr.esisar.calculatrice.CalculatriceException;
/**
 * 
 * @author clement
 *
 */
public interface Operation {
	/**
	 * Retourne le nom de l'opération
	 * @return
	 */
	public String getNom();
	
	/**
	 * Efffectue le calcule avec les opérandes
	 * @param operandes
	 * @return
	 * @throws CalculatriceException
	 */
	public Double calculer(Double... operandes) throws CalculatriceException;
	

}
