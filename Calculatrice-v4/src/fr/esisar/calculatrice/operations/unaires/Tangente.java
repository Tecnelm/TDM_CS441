package fr.esisar.calculatrice.operations.unaires;

import fr.esisar.calculatrice.operations.OperationUnaire;

/**
 * Réalise l'opération tangente
 * @author clement
 *
 */
public class Tangente extends OperationUnaire {

	@Override
	public String getNom()
	{
		return "tan";
	}
	
	@Override
	protected Double doCalculer(Double operande1) {
		
		return Math.tan(operande1);
	}
}
