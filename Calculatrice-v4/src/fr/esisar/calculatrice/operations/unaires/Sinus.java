package fr.esisar.calculatrice.operations.unaires;

import fr.esisar.calculatrice.operations.OperationUnaire;

/**
 * Réalise l'opération sinus 
 * @author clement
 *
 */
public class Sinus extends OperationUnaire {

	@Override
	public String getNom()
	{
		return "sin";
	}
	
	@Override
	protected Double doCalculer(Double operande1) {
		
		return Math.sin(operande1);
	}
}
