package fr.esisar.calculatrice.operations.unaires;

import fr.esisar.calculatrice.operations.OperationUnaire;

/**
 * Réalise l'opération cosinus
 * @author clement
 *
 */
public class Cosinus extends OperationUnaire{

	@Override
	public String getNom()
	{
		return "cos";
	}
	
	@Override
	protected Double doCalculer(Double operande1) {
		
		return Math.cos(operande1);
	}
}
