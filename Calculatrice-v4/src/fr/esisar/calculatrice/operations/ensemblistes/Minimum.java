package fr.esisar.calculatrice.operations.ensemblistes;

import fr.esisar.calculatrice.operations.OperationEnsembliste;

/**
 * Cherche le minimum dans une liste d'opérande passé en paramêtre
 * @author clement
 *
 */
public class Minimum extends OperationEnsembliste{

	
	@Override
	public String getNom()
	{
		return "min";
	}
	
	
	@Override
	protected Double doCalculer(Double... operandes) {
		
		Double min = operandes[0] ;
		for(Double val: operandes)
		{
			System.out.println(val);
			min = val <= min ? val : min;
		}
		return min;
	}
}
