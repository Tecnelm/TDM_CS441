package fr.esisar.calculatrice.operations.ensemblistes;

import fr.esisar.calculatrice.operations.OperationEnsembliste;

/**
 * Cherche le maximum dans la liste des operandes passé
 * @author clement
 *
 */
public class Maximum extends OperationEnsembliste{

	
	@Override
	public String getNom()
	{
		return "max";
	}
	
	@Override
	protected Double doCalculer(Double... operandes) {
		
		Double max = operandes[0] ;
		for(Double val: operandes)
		{
			max = val >= max ? val : max;
		}
		return max;
	}
}
