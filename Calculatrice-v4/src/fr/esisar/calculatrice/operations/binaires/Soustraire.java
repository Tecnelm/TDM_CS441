package fr.esisar.calculatrice.operations.binaires;

import fr.esisar.calculatrice.CalculatriceException;
import fr.esisar.calculatrice.operations.OperationBinaire;

/**
 * Effectue une soustraction
 * @author clement
 *
 */
public class Soustraire extends OperationBinaire {
	
	@Override
	public String getNom()
	{
		return "-";
	}

	@Override
	protected Double doCalculer(Double operande1, Double operande2) throws CalculatriceException {
		return operande1 - operande2;
	}
}
