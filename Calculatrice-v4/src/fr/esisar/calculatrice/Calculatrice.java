package fr.esisar.calculatrice;

import java.util.Set;

import fr.esisar.calculatrice.operations.Operation;

/**
 * 
 * @author clement
 *
 */
public class Calculatrice {
	/**
	 * 
	 */
	private Set<Operation> operationList;
	
	
	public Calculatrice() {
	}
	/**
	 * 
	 * @param operations
	 */
	public Calculatrice(Set<Operation> operations)
	{
		this.operationList = operations;
	}
	
	/**
	 * Rajoute une operation de la liste des opérations possible 
	 * @param operation Operation a enlever de la liste d'opération
	 */
	public void ajouterOperation(Operation operation)
	{
		this.operationList.add(operation);
	}
	
	/**
	 * Enlève une operation de la liste des opérations possible 
	 * @param operation Operation a enlever de la liste d'opération
	 */
	public void retirerOperation(Operation operation)
	{
		this.operationList.remove(operation);
	}
	
	/**
	 * Cherche l'opération correspondant au nom et la retourne
	 * 
	 * @param nom str that correspond to the operation
	 * @return Operation correspondant au nom
	 * @throws CalculatriceException en cas d'operation inexistante
	 */
	public Operation chercherOperation(String nom) throws CalculatriceException {
		return this.operationList.stream()
				.filter(op -> op.getNom().equals(nom))
				.findFirst()
				.orElseThrow(() -> new CalculatriceException("Operation inexistante dans la calculatrice"));
	}
	/**
	 * 
	 * @param nom Nom de l'opération à réaliser 
	 * @param operandes liste d'opérandes pour l'opération
	 * @return La valeur de l'opération
	 * @throws CalculatriceException
	 */
	public Double calculer(String nom,Double... operandes) throws CalculatriceException
	{
		Operation op = this.chercherOperation(nom);
		return op.calculer(operandes);
	}
	
	public Set getOperation()
	{
		return this.operationList;
	}
	
	
	
	

}
